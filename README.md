## 平台简介

OM-Admin后台管理系统

## 技术框架
1. 前端：jquery+bootstrap+adminlte3(vue 后续)
2. 后端:springboot+spring security+jwt+jpa+mybatis

## 内置功能

1.  用户管理：
2.  部门管理：
4.  菜单管理：
5.  角色管理：


## 在线体验



## 演示图

<table>
    <tr>
        <td><img src="https://gitee.com/onemy/om-admin/raw/master/doc/pic/homepage.png"/></td>
        <td><img src="https://gitee.com/onemy/om-admin/raw/master/doc/pic/user.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/onemy/om-admin/raw/master/doc/pic/auth.png"/></td>
        <td><img src="https://gitee.com/onemy/om-admin/raw/master/doc/pic/role.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/onemy/om-admin/raw/master/doc/pic/menu.png"/></td>
        <td><img src="https://gitee.com/onemy/om-admin/raw/master/doc/pic/group.png"/></td>
    </tr>    
</table>


## 交流群

QQ:1080825387